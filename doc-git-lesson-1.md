# Git Documentation

## Git basic commands

```bash
git init
git add file
git commit -m "comments"
git log --names-only
git checkout xxxxxxxx
git checkout xxxxxxxx -- file.name

git push
git pull
```
